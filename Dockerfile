FROM cypress/base:12.1.0

# avoid too many progress messages
# https://github.com/cypress-io/cypress/issues/1243
ENV CI=1
ARG CYPRESS_VERSION="9.3.1"
COPY cypress-demo cypress-demo
RUN mv cypress-demo/* ./
RUN npm install -g "cypress@${CYPRESS_VERSION}"
RUN cypress verify

# Cypress cache and installed version
RUN cypress cache path
RUN cypress cache list

ENTRYPOINT ["cypress", "run"]